#!/bin/bash

chown -R mysql:mysql /var/lib/mysql /var/run/mysqld
service mysql start

echo 'loading downloaded gz file to database'
tar -vzxOf /root/import.sql.tar.gz | mysql --default-character-set=utf8 && rm /root/import.sql.tar.gz

echo 'creating user and password, with full access'
echo "CREATE USER '$user' IDENTIFIED BY '$password';GRANT ALL PRIVILEGES ON *.* TO '$user'@'%' WITH GRANT OPTION; FLUSH PRIVILEGES;" | mysql --default-character-set=utf8
service mysql stop

echo 'finished loading database'
